import { Injectable } from "@nestjs/common";
import { Concert } from '../domain/interfaces/concerts.interface';

@Injectable()
export class ConcertsService {
    private readonly concerts: Concert[] = [];

    create(concert: Concert) {
        this.concerts.push(concert);
    }

    findAll(): Concert[] {
        return this.concerts;
    }
}