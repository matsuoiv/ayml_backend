import { Module } from "@nestjs/common";
import { ConcertsController } from "./concerts.controller";
import { ConcertsService } from "../application/concerts.service";

@Module({
    controllers: [ConcertsController],
    providers: [ConcertsService],
    exports: [ConcertsService]
})
export class ConcertsModule { }