export class CreateConcertDto {
    readonly place: string;
    readonly date: Date;
    readonly event: string;
}