import { Module } from '@nestjs/common';
import { ConcertsModule } from './concerts/infrastructure/concerts.module';

@Module({
  imports: [ConcertsModule],
})
export class AppModule { }
