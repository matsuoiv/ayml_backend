export interface Concert {
    readonly place: string;
    readonly date: Date;
    readonly event: string;
}