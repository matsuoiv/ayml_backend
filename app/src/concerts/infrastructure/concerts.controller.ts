import { Controller, Get, Post, Param, Body, Res, HttpStatus } from '@nestjs/common';

import { CreateConcertDto } from '../domain/dto/create-concert.dto';
import { ConcertsService } from '../application/concerts.service';
import { Concert } from '../domain/interfaces/concerts.interface';

@Controller('concerts')
export class ConcertsController {
    constructor(private readonly concertsService: ConcertsService) { }

    @Get()
    async findAll(): Promise<Concert[]> {
        return this.concertsService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id) {
        return `This action returns a #${id} concert`
    }

    @Post()
    async create(@Body() createConcertDto: CreateConcertDto) {
        this.concertsService.create(createConcertDto);
    }
}